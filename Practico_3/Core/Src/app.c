/*
 * tasks.c
 *
 *  Created on: 14 jun. 2021
 *      Author: Ruiz, AlbarrAn
 */

#include "app.h"
#include "main.h"

#include "lcd16x2_i2c.h"

//#include "DIO.h"
//#include <cpu_core.h>
//#include <os.h>

/*
 *******************************************************
 * 					TASKS STACK
 *******************************************************
 */
static OS_STK TaskSecuenciaStk[APP_CFG_TASK_SECUENCIA_STK_SIZE];
static OS_STK TaskTecladoStk[APP_CFG_TASK_TECLADO_STK_SIZE];
static OS_STK TaskEstadoStk[APP_CFG_TASK_ESTADO_STK_SIZE];
static OS_STK TaskFrecuenciaStk[APP_CFG_TASK_FRECUENCIA_STK_SIZE];
static OS_STK TaskOsciladorStk[APP_CFG_TASK_OSCILADOR_STK_SIZE];

/*
 *******************************************************
 * 					TASKS DECLARE
 *******************************************************
 */
static void TaskSecuencia(void *p_arg);
static void TaskTeclado(void *p_arg);
static void TaskEstado(void *p_arg);
static void TaskFrecuencia(void *p_arg);
static void TaskOscilador(void *p_arg);

static void App_TaskCreate (void);

/*******************************************************
 * 				VARIABLES EXTERNAS
 *******************************************************
 */
/********* Declarado en main.c por el IDE ********/
extern I2C_HandleTypeDef hi2c1;
extern TIM_HandleTypeDef htim2;

/*
 *******************************************************
 * 		    	EVENTOS uC/OS
 *******************************************************
 */
OS_EVENT *FrecPromMbox;			/* MailBox  		*/
OS_EVENT *SecMbox;				/* MailBox  		*/
OS_EVENT *SecEstMbox;			/* MailBox  		*/

OS_EVENT *OscStartSem;			/* Semaforo 		*/
OS_EVENT *OscStopSem;			/* Semaforo 		*/

/******************** EXTERN ************************/
// 			EVENTOS declarado en main.c
extern OS_EVENT *FrecMbox;		/* MailBox  		*/

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
void StartupTask (void *p_arg)
{
	CPU_INT32U cpu_clk;
	(void)p_arg;
	cpu_clk = HAL_RCC_GetHCLKFreq();
	/* Initialize and enable System Tick timer */
	OS_CPU_SysTickInitFreq(cpu_clk);

#if (OS_TASK_STAT_EN > 0)
	OSStatInit();                                               /* Determine CPU capacity.                              */
#endif

	/***************** EVENTOS **********************/
	FrecMbox = OSMboxCreate((void *) 0);						/* Crea buzon de msj */
	FrecPromMbox = OSMboxCreate((void *) 0);
	SecEstMbox = OSMboxCreate((void *) 0);
	SecMbox = OSMboxCreate((void *) 0);

	OscStartSem = OSSemCreate(0);		 						/* Crea un semaforo */
	OscStopSem = OSSemCreate(0);
	/************************************************/

	// App_EventCreate();                                         /* Create application events.                           */
	App_TaskCreate();                                           /* Create application tasks.                            */

	lcd16x2_i2c_init(&hi2c1);									/* Inicializa I2C */

	lcd16x2_i2c_clear();

/*
*************************************************************************
* Inicializa DIO
*************************************************************************
*/
	DICfgMode(DIO_SELSIN, DI_MODE_DIRECT);
	DICfgMode(DIO_SEL1, DI_MODE_DIRECT);
	DICfgMode(DIO_SEL2, DI_MODE_DIRECT);
	DICfgMode(DIO_SEL3, DI_MODE_DIRECT);

	DOCfgMode(DIO_SAL1, DO_MODE_BLINK_ASYNC, FALSE); 	//
	DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 19); 	//
	DOCfgMode(DIO_SAL2, DO_MODE_BLINK_ASYNC, FALSE); 	//
	DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 19, 39); 	//
	DOCfgMode(DIO_SAL3, DO_MODE_BLINK_ASYNC, FALSE); 	// Blink Secuencia 1
	DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 0, 39); 	//
	DOSet(DIO_SAL1, TRUE);								// al período y al tiempo en alto
	DOSet(DIO_SAL2, TRUE);								// se le resta el delay de DIOTask
	DOSet(DIO_SAL3, FALSE);								//

	DOCfgMode(DIO_LED, DO_MODE_BLINK_ASYNC, FALSE);		/****************************/
	DOCfgBlink(DIO_LED, DO_BLINK_EN, 50, 100);			/***** ESTOY VIVO !!!! ******/
/*************************************************************************
*/
	OSTaskDel(OS_PRIO_SELF);							/* HARAKIRI  :( */

//	while (DEF_TRUE)
//	{
////		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
//		OSTimeDlyHMSM(0, 0, 0, 500);
//	}

}// StartupTask

/**
 *********************************************************************************************************
 *                                          App_TaskCreate()
 *
 * Description : Crea las tareas.
 *
 * Argument(s) : void
 *
 * Return(s)   : none.
 *
 * Caller(s)   : StartupTask()
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
static void App_TaskCreate (void)
{
    CPU_INT08U os_err;

    os_err = OSTaskCreateExt((void (*)(void *)) TaskSecuencia,
                             (void          * ) 0,
                             (OS_STK        * )&TaskSecuenciaStk[APP_CFG_TASK_SECUENCIA_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_SECUENCIA_PRIO,
                             (INT16U          ) APP_CFG_TASK_SECUENCIA_PRIO,
                             (OS_STK        * ) &TaskSecuenciaStk[0],
                             (INT32U          ) APP_CFG_TASK_SECUENCIA_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_SECUENCIA_PRIO, (INT8U *)"Task Secuencia", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskTeclado,
                             (void          * ) 0,
                             (OS_STK        * )&TaskTecladoStk[APP_CFG_TASK_TECLADO_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_TECLADO_PRIO,
                             (INT16U          ) APP_CFG_TASK_TECLADO_PRIO,
                             (OS_STK        * ) &TaskTecladoStk[0],
                             (INT32U          ) APP_CFG_TASK_TECLADO_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_TECLADO_PRIO, (INT8U *)"Task Teclado", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskEstado,
                             (void          * ) 0,
                             (OS_STK        * )&TaskEstadoStk[APP_CFG_TASK_ESTADO_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_ESTADO_PRIO,
                             (INT16U          ) APP_CFG_TASK_ESTADO_PRIO,
                             (OS_STK        * ) &TaskEstadoStk[0],
                             (INT32U          ) APP_CFG_TASK_ESTADO_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_ESTADO_PRIO, (INT8U *)"Task Estado", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskFrecuencia,
                             (void          * ) 0,
                             (OS_STK        * )&TaskFrecuenciaStk[APP_CFG_TASK_FRECUENCIA_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_FRECUENCIA_PRIO,
                             (INT16U          ) APP_CFG_TASK_FRECUENCIA_PRIO,
                             (OS_STK        * ) &TaskFrecuenciaStk[0],
                             (INT32U          ) APP_CFG_TASK_FRECUENCIA_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_FRECUENCIA_PRIO, (INT8U *)"Task Frecuencia", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskOscilador,
                             (void          * ) 0,
                             (OS_STK        * )&TaskOsciladorStk[APP_CFG_TASK_OSCILADOR_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_OSCILADOR_PRIO,
                             (INT16U          ) APP_CFG_TASK_OSCILADOR_PRIO,
                             (OS_STK        * ) &TaskOsciladorStk[0],
                             (INT32U          ) APP_CFG_TASK_OSCILADOR_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_OSCILADOR_PRIO, (INT8U *)"Task Oscilador", &os_err);

}// App_TaskCreate

/*
 *********************************************************************************************************
 *                                          		TaskTeclado()
 *
 * Description : Recibe entradas del teclado
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
static void TaskTeclado (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U datos[1u];
//	CPU_INT08U Longitud;

	//CPU_BOOLEAN dio_in = DEF_FALSE;

	CPU_BOOLEAN flag = DEF_FALSE;
	CPU_BOOLEAN osc = DEF_FALSE;

	datos[0] = '1';

	while (DEF_TRUE)
	{
		OSTimeDlyHMSM(0u, 0u, 0u, 250u);

		if (DIGet(DIO_SEL1))   				// revisa entradas
		{
			flag = DEF_TRUE;
			datos[0] = '1';
			if (osc)
			{
				OSSemPost(OscStopSem);
				osc = DEF_FALSE;
			}
		}
		if (DIGet(DIO_SEL2))   				// revisa entradas
		{
			flag = DEF_TRUE;
			datos[0] = '2';
			if (osc)
			{
				OSSemPost(OscStopSem);
				osc = DEF_FALSE;
			}
		}
		if (DIGet(DIO_SEL3))   				// revisa entradas
		{
			flag = DEF_TRUE;
			datos[0] = '3';
			if (osc)
			{
				OSSemPost(OscStopSem);
				osc = DEF_FALSE;
			}
		}
		if (DIGet(DIO_SELSIN) && !(osc))   	// revisa entradas
		{
			flag = DEF_TRUE;
			datos[0] = 's';
			OSSemPost(OscStartSem);
			osc = DEF_TRUE;
		}

///////////////  PARA ENTRADA USB	////////////////////////////////////////////////////////////////////////////////
//		while ((Longitud = UsbRead(datos, 1u)) == 0u)     /* mientras no haya lectura del buffer de recepción USB */
//		{
//			OSTimeDlyHMSM(0u, 0u, 0u, 500u);
//		}
//		/* Si ingresa un dato por el USB			*/
//
//		if ((*datos == '1' || *datos == '2' || *datos == '3') && osc) // detiene secuencia
//		{
//			OSSemPost(OscStopSem);
//			osc = DEF_FALSE;
//		}
//		else if ((*datos == 's') && !(osc)) // si selecciona generador de secuencia
//		{
//			OSSemPost(OscStartSem);
//			osc = DEF_TRUE;
//		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (flag)
		{
			OSMboxPostOpt(SecMbox, (void *) &datos, OS_POST_OPT_NONE); // envia estado a TaskSecuencia
			flag = DEF_FALSE;
		}
	}

}// TaskTeclado

/*
 *********************************************************************************************************
 *                                          		TaskSecuencia()
 *
 * Description : Configuración de Secuencia
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
static void TaskSecuencia (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U err;
	CPU_INT08U *selec;

	while (DEF_TRUE)
	{
		selec = (CPU_INT08U *) OSMboxPend(SecMbox, 0, &err); // espera MailBox de Task teclado

		DOSet(DIO_SAL1, FALSE);
		DOSet(DIO_SAL2, FALSE);
		DOSet(DIO_SAL3, FALSE);
		OSMboxPost(SecEstMbox, (void *) selec);

		if (*selec == 's')									/* Sintetizador de señal					*/
		{
			OSTaskSuspend(OS_PRIO_SELF);					// Si esta en salida sintetizada se va a dormir
		}
		else if (*selec == '1')								/* Secuencia 1								*/
//		if (*selec == '1')									/* Secuencia 1								*/
		{
			OSTimeDlyHMSM(0u, 0u, 0u, 100u);

			DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 19);
			DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 19, 39);
			DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 0, 39);
			DOSet(DIO_SAL1, TRUE);
			DOSet(DIO_SAL2, TRUE);
			DOSet(DIO_SAL3, FALSE);
		}
		else if (*selec == '2')								/* Secuencia 2								*/
		{
			OSTimeDlyHMSM(0u, 0u, 0u, 100u);

			DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 39);
			DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 19, 39);
			DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 29, 39);
			DOSet(DIO_SAL1, TRUE);
			DOSet(DIO_SAL2, TRUE);
			DOSet(DIO_SAL3, TRUE);
		}
		else if (*selec == '3')								/* Secuencia 3								*/
		{
			OSTimeDlyHMSM(0u, 0u, 0u, 100u);

			DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 19);
			DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 14, 29);
			DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 19, 34);
			DOSet(DIO_SAL1, TRUE);
			DOSet(DIO_SAL2, TRUE);
			DOSet(DIO_SAL3, TRUE);
		}

	}
}// TaskSecuencia

/*
 *********************************************************************************************************
 *                                          		TaskEstado()
 *
 * Description : Imprime el estado
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
static void TaskEstado (void *p_arg)
{
	(void)p_arg;
	CPU_INT32U *frec_prom;
	CPU_INT32U frec;
	CPU_INT08U *sec_est;
	CPU_INT08U sec;

	while (DEF_TRUE)
	{
		OSTimeDlyHMSM(0u, 0u, 0u, 600u);

		if ((frec_prom = (CPU_INT32U *) OSMboxAccept(FrecPromMbox)) != NULL)  	// frecuencia x 10
		{
			frec = *frec_prom;
		}
		if ((sec_est = (CPU_INT08U *) OSMboxAccept(SecEstMbox)) != NULL) 		// secuencia actual
		{
			sec = *sec_est;
		}

		UsbPrintf("frec:%d\nsec:%c\n",        // envia frecuencia y estado por USB
				  frec,
				  sec);
		if (*sec_est == 's')				// Si esta en salida sintetizada se va a dormir
		{
			OSTaskSuspend(OS_PRIO_SELF);
		}
	}
}// TaskEstado

/*
 *********************************************************************************************************
 *                                          		TaskFrecuencia()
 *
 * Description : Promedia la frecuencia recibida de la IRQ y la manda a TaskEstado
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
static void TaskFrecuencia (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U err;
	CPU_INT08U cont;
	CPU_INT32U *frec;
	CPU_INT32U total = 0;
	CPU_INT32U promedio;

	while (DEF_TRUE){
		total = 0;
		cont = 0;
		for (CPU_INT08U i = 0; i < 10; i++)
		{
			OSTimeDlyHMSM(0u, 0u, 0u, 50u);
																			// recibe desde IRQ
			if ((frec = (CPU_INT32U *) OSMboxPend(FrecMbox, 50, &err)))  	// frecuencia x 10
			{
				total = total + *frec;
				++cont;
			}
		}
		if (cont > 0)
		{
			promedio =  total / cont;								// calcula un promedio
			OSMboxPost(FrecPromMbox, (void *) &promedio);			// envía MailBox
		}
	}

}// TaskFrecuencia

/*
 *********************************************************************************************************
 *                                          		TaskOscilador()
 *
 * Description : Activa o desactiva la salida de señal sintetizada por DMA
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
static void TaskOscilador (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U err;

	while (DEF_TRUE){
		OSSemPend(OscStartSem, 0, &err);
		OSTaskSuspend(APP_CFG_TASK_FRECUENCIA_PRIO);  // suspende la tarea de frecuencimetro
		__HAL_TIM_ENABLE_DMA(&htim2, TIM_DMA_UPDATE); // inicia el timer2 DMA
		OSSemPend(OscStopSem, 0, &err);
		__HAL_TIM_DISABLE_DMA(&htim2, TIM_DMA_UPDATE); // detiene el Timer2
		OSTaskResume(APP_CFG_TASK_FRECUENCIA_PRIO);		// resume tareas suspendidas
		OSTaskResume(APP_CFG_TASK_ESTADO_PRIO);
		OSTaskResume(APP_CFG_TASK_SECUENCIA_PRIO);
	}
}// TaskOscilador

/*************************************** FUNCIONES NO TASK **********************************************/

