/*
 * tasks.h
 *
 *  Created on: 14 jun. 2021
 *      Author:  */

#ifndef INC_APP_H_
#define INC_APP_H_

#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include "DIO.h"

/********** Es la única tarea que se llama desde main.c *********/
OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
void StartupTask(void *p_arg);

/*
 *************************************************************
 *					DIO DEFINES
 *************************************************************
 */
#define DIO_SAL1 	DIO_PB12
#define DIO_SAL2 	DIO_PB13
#define DIO_SAL3 	DIO_PB14
#define DIO_SELSIN 	DIO_PB11
#define DIO_SEL1 	DIO_PB3
#define DIO_SEL2 	DIO_PB4
#define DIO_SEL3 	DIO_PB5


#endif /* INC_APP_H_ */
