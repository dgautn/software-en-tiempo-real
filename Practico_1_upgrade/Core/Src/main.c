/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include <math.h>
#include "lib_str.h"
#include "stdio.h"
//#include "stdarg.h"
#include "strings.h"
//#include "UsbSerial.h"
#include "lcd16x2_i2c.h"

#include "DIO.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 16u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */
static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK TaskSec1Stk[APP_CFG_TASK_SEC_1_STK_SIZE];
static OS_STK TaskSec2Stk[APP_CFG_TASK_SEC_2_STK_SIZE];
static OS_STK TaskSec3Stk[APP_CFG_TASK_SEC_3_STK_SIZE];
static OS_STK TaskTecladoStk[APP_CFG_TASK_TECLADO_STK_SIZE];
static OS_STK TaskEstadoStk[APP_CFG_TASK_ESTADO_STK_SIZE];

OS_EVENT *RelojMbox;		/* MailBox  		*/

OS_EVENT *RstClkSem;		/* Semaforo 		*/

/******************************************************************************/
/*************************** Variables Globales *******************************/
/******************************************************************************/


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

static void StartupTask(void *p_arg);
static void TaskSec1(void *p_arg);
static void TaskSec2(void *p_arg);
static void TaskSec3(void *p_arg);
static void TaskTeclado(void *p_arg);
static void TaskEstado(void *p_arg);

static void App_TaskCreate (void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
	CPU_INT08U os_err;
#endif
	CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
	CPU_IntDis();
	for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
	{
		/* Set all external intr. to KA interrupt priority boundary */
		CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
	}
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

	OSInit();
	OSTaskCreateExt( StartupTask,
			0,
			&StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
			APP_CFG_STARTUP_TASK_PRIO,
			APP_CFG_STARTUP_TASK_PRIO,
			&StartupTaskStk[0],
			APP_CFG_STARTUP_TASK_STK_SIZE,
			0,
			(OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
#if (OS_TASK_NAME_EN > 0u)
			OSTaskNameSet( APP_CFG_STARTUP_TASK_PRIO,
			(INT8U *)"Startup task",
			&os_err);
#endif

	DIOInit();

	OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SAL1_Pin|SAL2_Pin|SAL3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ENT1_Pin ENT2_Pin */
  GPIO_InitStruct.Pin = ENT1_Pin|ENT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SAL1_Pin SAL2_Pin SAL3_Pin */
  GPIO_InitStruct.Pin = SAL1_Pin|SAL2_Pin|SAL3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/*
*************************************************************************
* STM32Cube HAL FUNCTIONS
*************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
 return (HAL_OK);
}

uint32_t HAL_GetTick(void)
{
 CPU_INT32U os_tick_ctr;

 #if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
 #else
  os_tick_ctr = OSTimeGet();
 #endif

 return os_tick_ctr;
}

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void StartupTask (void *p_arg)
{
	CPU_INT32U cpu_clk;
	(void)p_arg;
	cpu_clk = HAL_RCC_GetHCLKFreq();
	/* Initialize and enable System Tick timer */
	OS_CPU_SysTickInitFreq(cpu_clk);

#if (OS_TASK_STAT_EN > 0)
	OSStatInit();                                               /* Determine CPU capacity.                              */
#endif

	RelojMbox = OSMboxCreate((void *) 0);						/* Crea buzon de msj */

	RstClkSem = OSSemCreate(0);		 							/* Crea un semaforo */

	// App_EventCreate();                                         /* Create application events.                           */
	App_TaskCreate();                                             /* Create application tasks.                            */

	lcd16x2_i2c_init(&hi2c1);									/* Inicializa I2C */

	lcd16x2_i2c_clear();

	DICfgMode(DIO_PB10, DI_MODE_DIRECT);
	DICfgMode(DIO_PB11, DI_MODE_DIRECT);

	DOCfgMode(DIO_SAL1, DO_MODE_BLINK_ASYNC, FALSE);
	DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 19);
	DOCfgMode(DIO_SAL2, DO_MODE_BLINK_ASYNC, FALSE);
	DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 19, 39);
	DOCfgMode(DIO_SAL3, DO_MODE_BLINK_ASYNC, FALSE);
	DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 0, 39);
	DOSet(DIO_SAL1, TRUE);
	DOSet(DIO_SAL2, TRUE);
	DOSet(DIO_SAL3, FALSE);
//	DOCfgMode(DIO_LED, DO_MODE_DIRECT, FALSE);		/****************************/


	DOCfgMode(DIO_LED, DO_MODE_BLINK_ASYNC, FALSE);		/****************************/
	DOCfgBlink(DIO_LED, DO_BLINK_EN, 50, 100);			/***** ESTOY VIVO !!!! ******/

	OSTaskDel(APP_CFG_TASK_SEC_1_PRIO);
	OSTaskDel(APP_CFG_TASK_SEC_2_PRIO);
	OSTaskDel(APP_CFG_TASK_SEC_3_PRIO);
	OSTaskDel(OS_PRIO_SELF);
//	while (DEF_TRUE)
//	{
////		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
//		OSTimeDlyHMSM(0, 0, 0, 500);
//	}

}

/*
 *********************************************************************************************************
 *                                          App_TaskCreate()
 *
 * Description : Crea las tareas.
 *
 * Argument(s) : void
 *
 * Return(s)   : none.
 *
 * Caller(s)   : StartupTask()
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void App_TaskCreate (void)
{
    CPU_INT08U os_err;

    os_err = OSTaskCreateExt((void (*)(void *)) TaskSec1,
                             (void          * ) 0,
                             (OS_STK        * )&TaskSec1Stk[APP_CFG_TASK_SEC_1_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_SEC_1_PRIO,
                             (INT16U          ) APP_CFG_TASK_SEC_1_PRIO,
                             (OS_STK        * ) &TaskSec1Stk[0],
                             (INT32U          ) APP_CFG_TASK_SEC_1_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_SEC_1_PRIO, (INT8U *)"Task Sec 1", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskSec2,
                             (void          * ) 0,
                             (OS_STK        * )&TaskSec2Stk[APP_CFG_TASK_SEC_2_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_SEC_2_PRIO,
                             (INT16U          ) APP_CFG_TASK_SEC_2_PRIO,
                             (OS_STK        * ) &TaskSec2Stk[0],
                             (INT32U          ) APP_CFG_TASK_SEC_2_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_SEC_2_PRIO, (INT8U *)"Task Sec 2", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskSec3,
                             (void          * ) 0,
                             (OS_STK        * )&TaskSec3Stk[APP_CFG_TASK_SEC_3_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_SEC_3_PRIO,
                             (INT16U          ) APP_CFG_TASK_SEC_3_PRIO,
                             (OS_STK        * ) &TaskSec3Stk[0],
                             (INT32U          ) APP_CFG_TASK_SEC_3_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_SEC_3_PRIO, (INT8U *)"Task Sec 3", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskTeclado,
                             (void          * ) 0,
                             (OS_STK        * )&TaskTecladoStk[APP_CFG_TASK_TECLADO_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_TECLADO_PRIO,
                             (INT16U          ) APP_CFG_TASK_TECLADO_PRIO,
                             (OS_STK        * ) &TaskTecladoStk[0],
                             (INT32U          ) APP_CFG_TASK_TECLADO_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_TECLADO_PRIO, (INT8U *)"Task Teclado", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) TaskEstado,
                             (void          * ) 0,
                             (OS_STK        * )&TaskEstadoStk[APP_CFG_TASK_ESTADO_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_ESTADO_PRIO,
                             (INT16U          ) APP_CFG_TASK_ESTADO_PRIO,
                             (OS_STK        * ) &TaskEstadoStk[0],
                             (INT32U          ) APP_CFG_TASK_ESTADO_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_ESTADO_PRIO, (INT8U *)"Task Estado", &os_err);

}
/*
 *********************************************************************************************************
 *                                          		TaskSec1()
 *
 * Description :
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void TaskSec1 (void *p_arg)
{
	(void)p_arg;
	while (DEF_TRUE)
	{
		//DOSet(DIO_SAL1, !DOGet(DIO_SAL1));
		OSTimeDlyHMSM(0, 0, 1, 500);
	}
}

/*
 *********************************************************************************************************
 *                                          		TaskSec2()
 *
 * Description :
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void TaskSec2 (void *p_arg)
{
	(void)p_arg;
	while (DEF_TRUE)
	{
		OSTimeDlyHMSM(0, 0, 0, 500);
	}
}

/*
 *********************************************************************************************************
 *                                          		TaskSec3()
 *
 * Description :
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void TaskSec3 (void *p_arg)
{
	(void)p_arg;
	while (DEF_TRUE)
	{
		OSTimeDlyHMSM(0, 0, 0, 500);
	}
}

/*
 *********************************************************************************************************
 *                                          		TaskTeclado()
 *
 * Description :
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void TaskTeclado (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U datos[1u];
	CPU_INT08U Longitud;

//	CPU_BOOLEAN clockrun = DEF_TRUE;

	while (DEF_TRUE)
	{
		//		DOSet(DIO_LED, DIGet(DIO_ENT1));
		//		OSTimeDlyHMSM(0, 0, 0, 500);
		while ((Longitud = UsbRead(datos, 1u)) == 0u)     /* mientras no haya lectura del buffer de recepción USB */
		{
			OSTimeDlyHMSM(0u, 0u, 0u, 500u);
		}
		/* Si ingresa un dato por el USB			*/
		if (*datos == '1')								/* Secuencia 1								*/
		{
			DOSet(DIO_SAL1, FALSE);
			DOSet(DIO_SAL2, FALSE);
			DOSet(DIO_SAL3, FALSE);

			OSTimeDlyHMSM(0u, 0u, 0u, 100u);

			DOCfgMode(DIO_SAL1, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 19);
			DOCfgMode(DIO_SAL2, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 19, 39);
			DOCfgMode(DIO_SAL3, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 0, 39);
			DOSet(DIO_SAL1, TRUE);
			DOSet(DIO_SAL2, TRUE);
			DOSet(DIO_SAL3, FALSE);
		}
		else if (*datos == '2')								/* Secuencia 2								*/
		{
			DOSet(DIO_SAL1, FALSE);
			DOSet(DIO_SAL2, FALSE);
			DOSet(DIO_SAL3, FALSE);

			OSTimeDlyHMSM(0u, 0u, 0u, 100u);

			DOCfgMode(DIO_SAL1, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 39);
			DOCfgMode(DIO_SAL2, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 19, 39);
			DOCfgMode(DIO_SAL3, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 29, 39);
			DOSet(DIO_SAL1, TRUE);
			DOSet(DIO_SAL2, TRUE);
			DOSet(DIO_SAL3, TRUE);
		}
		else if (*datos == '3')								/* Secuencia 3								*/
		{
			DOSet(DIO_SAL1, FALSE);
			DOSet(DIO_SAL2, FALSE);
			DOSet(DIO_SAL3, FALSE);

			OSTimeDlyHMSM(0u, 0u, 0u, 100u);

			DOCfgMode(DIO_SAL1, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL1, DO_BLINK_EN_NORMAL, 9, 19);
			DOCfgMode(DIO_SAL2, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL2, DO_BLINK_EN_NORMAL, 14, 29);
			DOCfgMode(DIO_SAL3, DO_MODE_BLINK_ASYNC, FALSE);
			DOCfgBlink(DIO_SAL3, DO_BLINK_EN_NORMAL, 19, 34);
			DOSet(DIO_SAL1, TRUE);
			DOSet(DIO_SAL2, TRUE);
			DOSet(DIO_SAL3, TRUE);
		}
	}

}


/*
 *********************************************************************************************************
 *                                          		TaskEstado()
 *
 * Description :
 *
 * Argument(s) : p_arg
 *
 * Return(s)   : none.
 *
 * Caller(s)   : This is a task.
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void TaskEstado (void *p_arg)
{
	(void)p_arg;
	while (DEF_TRUE)
	{
		OSTimeDlyHMSM(0, 0, 0, 500);
	}
}

/*************************************** FUNCIONES NO TASK **********************************************/


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
