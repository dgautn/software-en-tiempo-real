/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
#include <math.h>
#include "lib_str.h"
#include "stdio.h"
//#include "stdarg.h"
#include "strings.h"
//#include "UsbSerial.h"
#include "lcd16x2_i2c.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 16u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */
static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK Task1BlinkStk[APP_CFG_TASK_1_BLINK_STK_SIZE];
static OS_STK Task2KeysStk[APP_CFG_TASK_2_KEYS_STK_SIZE];
static OS_STK Task3ClockStk[APP_CFG_TASK_3_CLOCK_STK_SIZE];
static OS_STK Task4LcdStk[APP_CFG_TASK_4_LCD_STK_SIZE];

OS_EVENT *RelojMbox;
OS_EVENT *UpMbox;
OS_EVENT *UpLcdMbox;

OS_EVENT *RstClkSem;

/******************************************************************************/
/*************************** Variables Globales *******************************/
/******************************************************************************/


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */
static void StartupTask(void *p_arg);
static void Task1Blink(void *p_arg);
static void Task2Keys(void *p_arg);
static void Task3Clock(void *p_arg);
static void Task4Lcd(void *p_arg);

static void App_TaskCreate (void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
CPU_INT08U os_err;
#endif
CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  CPU_IntDis();
  for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
  {
  /* Set all external intr. to KA interrupt priority boundary */
  CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
  }
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  OSInit();
  OSTaskCreateExt( StartupTask,
                   0,
				   &StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
				   APP_CFG_STARTUP_TASK_PRIO,
				   APP_CFG_STARTUP_TASK_PRIO,
				   &StartupTaskStk[0],
				   APP_CFG_STARTUP_TASK_STK_SIZE,
				   0,
				   (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));
  #if (OS_TASK_NAME_EN > 0u)
   OSTaskNameSet( APP_CFG_STARTUP_TASK_PRIO,
    	  	      (INT8U *)"Startup task",
				  &os_err);
  #endif

  OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TEST_GPIO_Port, TEST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : TEST_Pin */
  GPIO_InitStruct.Pin = TEST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TEST_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/*
*************************************************************************
* STM32Cube HAL FUNCTIONS
*************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
 return (HAL_OK);
}

uint32_t HAL_GetTick(void)
{
 CPU_INT32U os_tick_ctr;

 #if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
 #else
  os_tick_ctr = OSTimeGet();
 #endif

 return os_tick_ctr;
}

/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void StartupTask (void *p_arg)
{
	CPU_INT32U cpu_clk;
	(void)p_arg;
	cpu_clk = HAL_RCC_GetHCLKFreq();
	/* Initialize and enable System Tick timer */
	OS_CPU_SysTickInitFreq(cpu_clk);

#if (OS_TASK_STAT_EN > 0)
	OSStatInit();                                               /* Determine CPU capacity.                              */
#endif

	RelojMbox = OSMboxCreate((void *) 0);						/* Crea buzon de msj */
	UpMbox = OSMboxCreate((void *) 0);							/* Crea buzon de msj */
	UpLcdMbox = OSMboxCreate((void *) 0);						/* Crea buzon de msj */

	RstClkSem = OSSemCreate(0);		 							/* Crea un semaforo */

	// App_EventCreate();                                         /* Create application events.                           */
	App_TaskCreate();                                             /* Create application tasks.                            */

	lcd16x2_i2c_init(&hi2c1);									/* Inicializa I2C */

	lcd16x2_i2c_clear();

	OSTaskDel(OS_PRIO_SELF);
}

/*
 *********************************************************************************************************
 *                                          App_TaskCreate()
 *
 * Description : Crea las tareas.
 *
 * Argument(s) : void
 *
 * Return(s)   : none.
 *
 * Caller(s)   : StartupTask()
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */

static void App_TaskCreate (void)
{
    CPU_INT08U os_err;

    os_err = OSTaskCreateExt((void (*)(void *)) Task1Blink,
                             (void          * ) 0,
                             (OS_STK        * )&Task1BlinkStk[APP_CFG_TASK_1_BLINK_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_1_BLINK_PRIO,
                             (INT16U          ) APP_CFG_TASK_1_BLINK_PRIO,
                             (OS_STK        * ) &Task1BlinkStk[0],
                             (INT32U          ) APP_CFG_TASK_1_BLINK_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_1_BLINK_PRIO, (INT8U *)"Task 1 Blink", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) Task2Keys,
                             (void          * ) 0,
                             (OS_STK        * )&Task2KeysStk[APP_CFG_TASK_2_KEYS_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_2_KEYS_PRIO,
                             (INT16U          ) APP_CFG_TASK_2_KEYS_PRIO,
                             (OS_STK        * ) &Task2KeysStk[0],
                             (INT32U          ) APP_CFG_TASK_2_KEYS_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_2_KEYS_PRIO, (INT8U *)"Task 2 Keys", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) Task3Clock,
                             (void          * ) 0,
                             (OS_STK        * )&Task3ClockStk[APP_CFG_TASK_3_CLOCK_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_3_CLOCK_PRIO,
                             (INT16U          ) APP_CFG_TASK_3_CLOCK_PRIO,
                             (OS_STK        * ) &Task3ClockStk[0],
                             (INT32U          ) APP_CFG_TASK_3_CLOCK_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_3_CLOCK_PRIO, (INT8U *)"Task 3 Clock", &os_err);

    os_err = OSTaskCreateExt((void (*)(void *)) Task4Lcd,
                             (void          * ) 0,
                             (OS_STK        * )&Task4LcdStk[APP_CFG_TASK_4_LCD_STK_SIZE - 1],
                             (INT8U           ) APP_CFG_TASK_4_LCD_PRIO,
                             (INT16U          ) APP_CFG_TASK_4_LCD_PRIO,
                             (OS_STK        * ) &Task4LcdStk[0],
                             (INT32U          ) APP_CFG_TASK_4_LCD_STK_SIZE,
                             (void          * ) 0,
                             (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

    OSTaskNameSet( APP_CFG_TASK_4_LCD_PRIO, (INT8U *)"Task 4 LCD", &os_err);

}

/*
 ********************************************************************************************************
 *												   TASKS
 ********************************************************************************************************
 */

/*
*********************************************************************************************************
*                                          		Task1Blink()
*
* Description : Led indicador de Reloj Activo - cambia cada 500ms
*
* Argument(s) : p_arg
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Task1Blink (void *p_arg)
{
	(void)p_arg;

	while (DEF_TRUE){
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);					/* blink						*/
		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}

/*
*********************************************************************************************************
*                                          		Task2Keys()
*
* Description : Verifica teclas presionadas - Cada 500 ms
*
* Argument(s) : p_arg
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : S --> Inicio/Detención del reloj.
* 				R --> Reseteo de la cuenta.
* 				U --> Reloj Ascendente.
* 				D --> Reloj Descendente.
*********************************************************************************************************
*/

static void Task2Keys (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U datos[1u];
	CPU_INT08U Longitud;

	CPU_BOOLEAN clockrun = DEF_TRUE;

	while (DEF_TRUE){
		while ((Longitud = UsbRead(datos, 1u)) == 0u)     /* mientras no haya lectura del buffer de recepción USB */
		{
			OSTimeDlyHMSM(0u, 0u, 0u, 500u);
		}
														/* Si ingresa un dato por el USB			*/
		if (*datos == 's')								/* Start/Stop								*/
		{
			if (clockrun)								/* si esta en marcha el contador 			*/
			{
				clockrun = DEF_FALSE;
				OSTaskSuspend(APP_CFG_TASK_1_BLINK_PRIO);
				OSTaskSuspend(APP_CFG_TASK_3_CLOCK_PRIO);
			}
			else										/* si esta detenido							*/
			{
				clockrun = DEF_TRUE;
				OSTaskResume(APP_CFG_TASK_1_BLINK_PRIO);
				OSTaskResume(APP_CFG_TASK_3_CLOCK_PRIO);
			}
		}
		else if (*datos == 'r')							/* resetea el contador						*/
		{
			OSSemPost(RstClkSem);
		}
		else if (*datos == 'u')							/* ascendente								*/
		{
			OSMboxPostOpt(UpMbox, (void *) &datos, OS_POST_OPT_NONE);
		}
		else if (*datos == 'd')							/* descendente								*/
		{
			OSMboxPostOpt(UpMbox, (void *) &datos, OS_POST_OPT_NONE);
		}
	}
}

/*
*********************************************************************************************************
*                                          		Task3Clock()
*
* Description : Incrementa/Decrementa el valor del reloj cada 100mSegundos.
*
* Argument(s) : p_arg
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Task3Clock (void *p_arg)
{
	(void)p_arg;
	CPU_INT32U tiempo = 0u;							/*	Inicia el reloj								*/

	CPU_BOOLEAN updown = DEF_TRUE;					/*	Inicia como cuenta ascendente				*/
	CPU_INT08U *rxUpdown;

	CPU_INT08U charTx;

	while (DEF_TRUE){
		if ((rxUpdown = (CPU_INT08U *) OSMboxAccept(UpMbox)) != NULL)
		{
			if (*rxUpdown == 'u')
			{
				updown = DEF_TRUE;
				charTx= 'u';
			}
			else if (*rxUpdown == 'd')
			{
				updown = DEF_FALSE;
				charTx= 'd';
			}
			OSMboxPostOpt(UpLcdMbox, (void *) &charTx, OS_POST_OPT_NONE);
		}
		if ( OSSemAccept(RstClkSem) )
		{
			tiempo = 0;
		}
		if (updown)											/*	Cuenta ascendente							*/
		{
			HAL_GPIO_TogglePin(TEST_GPIO_Port, TEST_Pin);	/* test point 									*/
			tiempo = tiempo + 100u;
			if (tiempo >= (12u * 60u) * 1000u)				/*	12 min 										*/
			{
				tiempo = 0u;
			}
		}
		if (!updown)										/*	Cuenta descendente							*/
		{
			HAL_GPIO_TogglePin(TEST_GPIO_Port, TEST_Pin);	/* test point 									*/
			if (tiempo >= 100u)
			{
				tiempo = tiempo - 100u;
			}
			else
			{
				tiempo = (12u * 60u) * 1000u;				/*	12 min 										*/
			}
		}
		OSMboxPost(RelojMbox, (void *) &tiempo);
		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

/*
*********************************************************************************************************
*                                          		Task4Lcd()
*
* Description : Muestra en LCD los datos del RELOJ
*
* Argument(s) : p_arg
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void Task4Lcd (void *p_arg)
{
	(void)p_arg;
	CPU_INT08U err;
	CPU_INT32U *reloj;
	CPU_BOOLEAN isUp = DEF_TRUE;									/*	Inicia ascendente ???	*/
	CPU_INT08U *rxIsUp;

	CPU_INT16U min;
	CPU_INT16U seg;
	CPU_INT16U mseg;

//	OSTimeDlyHMSM(0u, 0u, 0u, 500u);

	while (DEF_TRUE){
		if ((rxIsUp = (CPU_INT08U *) OSMboxAccept(UpLcdMbox)) != NULL)
		{
			if (*rxIsUp == 'u')
				isUp = DEF_TRUE;
			if (*rxIsUp == 'd')
				isUp = DEF_FALSE;
		}
		reloj = (CPU_INT32U *) OSMboxPend(RelojMbox, 0, &err);
		min = *reloj / 60u / 1000u;									/* minutos						*/
		seg = *reloj / 1000u - (min * 60);							/* segundos						*/
		mseg = *reloj % 1000u;										/* milisegundos					*/
		lcd16x2_i2c_clear();
		lcd16x2_i2c_1stLine();
		lcd16x2_i2c_printf(isUp ? "   Ascendente" : "  Descendente"); /* primera linea 				*/
		lcd16x2_i2c_2ndLine();
		lcd16x2_i2c_printf("    %02d:%02d:%d ", min, seg, mseg / 100);	/* segunda linea - contador		*/
		UsbPrintf("OSTaskCtr:%d\n", OSTaskCtr);
		UsbPrintf("OSCPUUsage:%d\n", OSCPUUsage);
		UsbPrintf("Time:%d\n", *reloj);
//		UsbPrintf("Up:%d\n", isUp ? 1 : 0);
		//UsbPrintf("%02d:%02d:%d\n", min, seg, mseg / 100);
		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}
/*************************************** FUNCIONES NO TASK **********************************************/


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
