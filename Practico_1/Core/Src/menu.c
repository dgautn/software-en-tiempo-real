/*
 * menu.c
 *
 *  Created on: Apr 20, 2021
 *      Author: Ruiz - Albarran
 */

#include "main.h"
#include "menu.h"

/*
**************************************************************************************************************************
*                                               menu()
*
* Description : menu para la seleccion de la secuencia
* Argument(s) : none
* Return(s)   : 0
* Caller(s)   :	main()
* Note(s)     : se selecciona pulsando 1, 2 o 3 desde el teclado.
* 				llama a las funciones que genera las secuencias y luego sale
* 				pulsando otra tecla sale imprimiendo mensaje de error y sin llamar a ninguna funcion
**************************************************************************************************************************
*/
extern char secu;

int menu(void)
{
	uint8_t Datos[1], Longitud;

	HAL_GPIO_WritePin(SAL1_GPIO_Port, SAL1_Pin, GPIO_PIN_RESET); /* Reset inicial */
	HAL_GPIO_WritePin(SAL2_GPIO_Port, SAL2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAL3_GPIO_Port, SAL3_Pin, GPIO_PIN_RESET);

	UsbPrintf("\nSeleccione una secuencia (1, 2, 3): ");
	while ((Longitud = UsbRead(Datos, 1)) == 0)     /* mientras no haya lectura del buffer de recepción USB */
	{
		HAL_Delay(50);
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
	}
	// Si ingresa un dato por el USB
	HAL_Delay(100);
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
	secu = *Datos;

	if (*Datos == '1')
	{
		UsbPrintf("%c \n", *Datos);        /* Reenvio los datos recibidos. 							     */
		secuencia1();
	}
	else if (*Datos == '2')
	{
		UsbPrintf("%c \n", *Datos);
		secuencia2();
	}
	else if (*Datos == '3')
	{
		UsbPrintf("%c \n", *Datos);
		secuencia3();
	}
	else
	{
		UsbPrintf("%c \nseleccion incorrecta", *Datos);
		HAL_Delay(100);
	}

	return 0;
}

