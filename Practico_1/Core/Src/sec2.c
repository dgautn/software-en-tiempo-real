/*
 * sec2.c
 *
 *  Created on: Apr 20, 2021
 *      Author: Ruiz - Albarran
 */

#include "main.h"
#include "sec2.h"

/*
**************************************************************************************************************************
*                                               secuencia2()
*
* Description :	Funcion para generar la secuencia 2
* Argument(s) : none
* Return(s)   : 0
* Caller(s)   :	menu()
* Note(s)     : pines de salida: SAL1_Pin, SAL1_Pin, SAL1_Pin.
* 				salida 1: [periodo 20 unidades de tiempo] [10 - alto] [30 - bajo]
* 				salida 2: [periodo 40 unidades de tiempo] [20 - alto] [20 - bajo]
* 				salida 3: [periodo 40 unidades de tiempo] [30 - alto] [10 - bajo]
* 				periodo comun: 40 unidades de tiempo
**************************************************************************************************************************
*/

extern char secu;

int secuencia2(void){

	uint32_t timeCount = 0; // contador para el tiempo
	uint8_t Datos[1], Longitud;

	UsbPrintf("\nSecuencia %c activada - presione una tecla para salir\n", secu);
	while (1)
	{

	  Longitud = UsbRead(Datos, 1);     /* Lectura del buffer de recepción de datos USB CDC.               */
	  if (Longitud == 0)
	   {
		  // generacion de secuencia 2
		  if (((timeCount % 40)==10)||((timeCount % 40)==0)) HAL_GPIO_TogglePin(SAL1_GPIO_Port, SAL1_Pin);
		  if ((timeCount % 20)==0) HAL_GPIO_TogglePin(SAL2_GPIO_Port, SAL2_Pin);
		  if (((timeCount % 40)==30)||((timeCount % 40)==0)) HAL_GPIO_TogglePin(SAL3_GPIO_Port, SAL3_Pin);
		  HAL_Delay(5 * U_TIEMPO); // espera pasiva en unidades de tiempo
		  timeCount += 5;
		  if(timeCount >= 40)timeCount = 0; // MCM de los periodos
	   }
	  else                                /* Si llegaron datos.  											 */
	   {
		  return 0;
	   }


	}


}
