#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#
# script Python: create.py
# (c) 2021, Gusatvo AlbarrAn
#
# GeneraciOn de plantillas para la creaciOn de Tasks uCOS

try:
    tareas = []
    i = 0
    print()

# Ingresa la lista de tareas    
    while(True):
        print('\nIngrese la tarea ', len(tareas) + 1, end=' (o presione enter para finalizar): ')
        tarea = input()
        if tarea != '':
            i = 1
            tareas.append([tarea, '', ''])
        else:
            break
    assert i > 0

# Crea los nombres
    for tarea in tareas:
        tarea[1] = tarea[0].title().replace(' ', '')
        tarea[2] = tarea[0].upper().replace(' ', '_')

# Abre los archivos 
    plantilla = open("_taskcreate.template", "rt")
    plantilladef = open("_taskdefine.template", "rt")
    hTemp = open("stack_prio_app_cfg.txt", "wt")
    cTemp = open("taskcreate.txt", "wt")
    cTempProto = open("task_prototipos.txt", "wt")
    cTempTask = open("task_definiciones.txt", "wt")
    plantLineas = plantilla.readlines()
    plantDefLineas = plantilladef.readlines()

# Crea el .txt para pegar en App_TaskCreate
    cTemp.write('''
/*
 *********************************************************************************************************
 *                                          App_TaskCreate()
 *
 * Description : Crea las tareas.
 *
 * Argument(s) : void 
 *
 * Return(s)   : none.
 *
 * Caller(s)   : StartupTask()
 *
 * Note(s)     : none.
 *********************************************************************************************************
 */
''')
    cTemp.write('\nstatic void App_TaskCreate (void)\n{\n    CPU_INT08U os_err;\n\n')
    for tarea in tareas:
        for linea in plantLineas:
            cTemp.write(linea.replace('<<Tarea Uno>>',tarea[0]).replace('<<TareaUno>>',tarea[1]).replace('<<TAREA_UNO>>',tarea[2]))
    cTemp.write('}')

# Crea el .txt para pegar en los prototipos de funcion
    cTempProto.write('static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];\n')
    for tarea in tareas:
        cTempProto.write('static OS_STK ')
        cTempProto.write(tarea[1])
        cTempProto.write('Stk[APP_CFG_')
        cTempProto.write(tarea[2])
        cTempProto.write('_STK_SIZE];\n')
    cTempProto.write('\nstatic void StartupTask(void *p_arg);\n')
    for tarea in tareas:
        cTempProto.write('static void ')
        cTempProto.write(tarea[1])
        cTempProto.write('(void *p_arg);\n')
    cTempProto.write('\nstatic void App_TaskCreate (void);')

# Crea el .txt para pegar en app_cfg.h
    hTemp.write('''
/*
*********************************************************************************************************
*                                           TASK PRIORITIES
*********************************************************************************************************
*/
''')
    hTemp.write('\n#define  APP_CFG_STARTUP_TASK_PRIO\t\t3u\n')
    i = 4
    for tarea in tareas:
        hTemp.write('#define  APP_CFG_')
        hTemp.write(tarea[2])
        hTemp.write('_PRIO\t\t')
        hTemp.write(str(i))
        i += 1
        hTemp.write('u\n')
    hTemp.write('\n#define  OS_TASK_TMR_PRIO\t\t\t(OS_LOWEST_PRIO - 2u)\n\n')
    hTemp.write('''
/*
*********************************************************************************************************
*                                          TASK STACK SIZES
*                             Size of the task stacks (# of OS_STK entries)
*********************************************************************************************************
*/
''')
    hTemp.write('\n#warning "Modificar tamaños de stack"\n')
    hTemp.write('#define  APP_CFG_STARTUP_TASK_STK_SIZE\t\t128u\n')
    for tarea in tareas:
        hTemp.write('#define  APP_CFG_')
        hTemp.write(tarea[2])
        hTemp.write('_STK_SIZE\t\t128u\n')

# Crea el .txt para pegar en las definiciones de las tareas
    for tarea in tareas:
        for linea in plantDefLineas:
            cTempTask.write(linea.replace('<<TareaUno>>',tarea[1]))

# Cierra los archivos 
    plantilla.close()
    hTemp.close()
    cTemp.close()
    cTempProto.close()
    cTempTask.close()

# Manejo de excepciones
except AssertionError:
    print('Sin datos')
except:
    print('Datos erroneos')
