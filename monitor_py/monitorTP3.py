#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# script Python: monitorTP1.upgrade.py
# (c) 2021, Gustavo AlbarrAn

import sys
import curses
import serial

def monitor(screen):
    frec = 0        # 
    sec = ''        # variables p recibir p USB

    begin_x = 20    # pos inicial ventana
    begin_y = 7
    height = 12     # tamaño inicial ventana
    width = 43
    
                    # peres color back/front
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.curs_set(False)  # no muestra cursor
    
                    # ventana principal
    win = curses.newwin(height, width, begin_y, begin_x)
    win.bkgd(' ', curses.color_pair(1))
    win.box()
    win.nodelay(True)   # no espera en getch()
    win.addstr(1, 13,"Monitor  BluePill")
    
                    # ventana sec 1        
    win1 = win.subwin(3, 13, begin_y + 2, begin_x + 1)
    win1.bkgd(' ', curses.color_pair(3))
    win1.box()
    win1.addstr(1, 1,"Secuencia 1", curses.A_BOLD)

                    # ventana sec 2        
    win2 = win.subwin(3, 13, begin_y + 2, begin_x + 15)
    win2.bkgd(' ', curses.color_pair(2))
    win2.box()
    win2.addstr(1, 1,"Secuencia 2", curses.A_BOLD)

                    # ventana sec 3        
    win3 = win.subwin(3, 13, begin_y + 2, begin_x + 29)
    win3.bkgd(' ', curses.color_pair(2))
    win3.box()
    win3.addstr(1, 1,"Secuencia 3", curses.A_BOLD)

                    # ventana frecuencimetro        
    winfrec = win.subwin(3, 23, begin_y + 5, begin_x + 10)
    winfrec.bkgd(' ', curses.color_pair(2))
    winfrec.box()
    winfrec.addstr(1, 1,"Frecuencia:")
    
                    # ventana señal sintetizada 
    winosc = win.subwin(3, 21, begin_y + 8, begin_x + 11)
    winosc.bkgd(' ', curses.color_pair(2))
    winosc.box()
    winosc.addstr(1, 1, "Salida  Sintetizada", curses.A_BOLD)
    
    while(True):
        ser_bytes = ser.readline()          # recibe por USB
        ser_str = ser_bytes.decode('UTF-8') 
        lines = ser_str.splitlines()        # separa en lineas
        
        for line in lines:          # recorre cada linea recibida
            if 'frec' in line:      # identifica el dato de frecuencia
                x = line.index(':')
                frec = line[x + 1 :]
                if frec.isdigit:
                    frec = float(frec) / 10     # pasa a float y divide por 10
            elif 'sec' in line:     # identifica la secuencia actual
                x = line.index(':')
                sec = line[x + 1 :]

        winfrec.addstr(1, 14,"", curses.A_BOLD) # ?? no me acuerdo 
        if frec < 10000:            # si es menor a 10 kHz muestra
            winfrec.addstr(1, 13, '{:6.1f} Hz'.format(frec), curses.A_BOLD)
        else:
            winfrec.addstr(1, 13, '  ------ ')

        c = win.getch()             # recibe caracter del teclado
        if c != -1 and ( (car := chr(c)) in ['q', 'Q', '1', '2', '3', 's'] ):
            if car in ['q', 'Q']:   # sale
                break

            ser.write(bytes(car, 'utf-8'))  # envia a la BluePill

        clr = [2 for x in range(4)]     # vector para cambiar color a ventana actual
#            if car in ['q', 'Q']:
#                break
#            elif car in ['s', 'S']:
#                clr[3] = 3
##                frec = 65535
#            elif car in ['1', '2', '3']:
#                clr[int(car)-1] = 3
#            else:
#                pass
        if sec == 's':      # salida sintetizada
            clr[3] = 3      
            frec = 65535    # si esta en salida sintetizada no muestra frecuencia
        elif sec in ['1', '2', '3']:
            clr[int(sec)-1] = 3         # arma par de colores p ventanas
#            else:
#                pass
        win1.bkgd(' ', curses.color_pair(clr[0]))   # carga pares de colores
        win2.bkgd(' ', curses.color_pair(clr[1]))
        win3.bkgd(' ', curses.color_pair(clr[2]))
        winosc.bkgd(' ', curses.color_pair(clr[3]))

        win.refresh()       # refresca ventanas
        win1.refresh()
        win2.refresh()
        win3.refresh()
        winfrec.refresh()
        winosc.refresh()

    curses.napms(200)       # pausa 200 ms

try: 
    assert len(sys.argv) > 1, 'Especificar puerto'
    port = sys.argv[1]                          # dev puerto
    ser = serial.Serial(port, timeout=250/1000) # conecta. timeout 200 ms
    ser.flushInput()
    curses.wrapper(monitor)     # función curses
    ser.close()                 # cierra puerto
except AssertionError as msg: # si no se indica puerto
    print(msg)
except:                         # otro error
    print('Error al leer el puerto')
    raise
    ser.close()
