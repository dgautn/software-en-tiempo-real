#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# script Python: monitorTP1.upgrade.py
# (c) 2021, Gustavo AlbarrAn

import sys
import curses
import serial

def monitor(screen):
    cpu = 0
    task = 0
    time = 0
    up = 0

    begin_x = 20
    begin_y = 7
    height = 6
    width = 43
    
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.curs_set(False)  # no muestra cursor
    
    win = curses.newwin(height, width, begin_y, begin_x)
    win.bkgd(' ', curses.color_pair(1))
    win.box()
    win.nodelay(True)   # no espera en getch()
    win.addstr(1, 13,"Monitor BluePill", curses.A_BOLD)

    win1 = win.subwin(3, 13, begin_y + 2, begin_x + 1)
    win1.bkgd(' ', curses.color_pair(3))
    win1.box()
    win1.addstr(1, 1,"Secuencia 1", curses.A_BOLD)

    win2 = win.subwin(3, 13, begin_y + 2, begin_x + 15)
    win2.bkgd(' ', curses.color_pair(2))
    win2.box()
    win2.addstr(1, 1,"Secuencia 2", curses.A_BOLD)

    win3 = win.subwin(3, 13, begin_y + 2, begin_x + 29)
    win3.bkgd(' ', curses.color_pair(2))
    win3.box()
    win3.addstr(1, 1,"Secuencia 3", curses.A_BOLD)
    
    while(True):
#        ser_bytes = ser.readline()
#        ser_str = ser_bytes.decode('UTF-8')
#
#        if 'OSTaskCtr' in ser_str:
#            x = ser_str.index(':')
#            task = ser_str[x + 1 : -1]
#            task = int(task)
#
#        if 'OSCPUUsage' in ser_str:
#            x = ser_str.index(':')
#            cpu = ser_str[x + 1 : -1]
#            cpu = int(cpu)
#
#        if 'Time' in ser_str:
#            x = ser_str.index(':')
#            time = ser_str[x + 1 : -1]
#            time = int(time)
#
#        if 'Up' in ser_str:
#            x = ser_str.index(':')
#            up = ser_str[x + 1 : -1]
#            up = int(up)

        c = win.getch()
#        if c != -1:
        if c != -1 and ( (car := chr(c)) in ['q', '1', '2', '3'] ):
            if car == 'q':
                break
            clr = [2 for x in range(3)]
            clr[int(car)-1] = 3
            win1.bkgd(' ', curses.color_pair(clr[0]))
            win2.bkgd(' ', curses.color_pair(clr[1]))
            win3.bkgd(' ', curses.color_pair(clr[2]))
            ser.write(bytes(car, 'utf-8'))
        win.refresh()
        win1.refresh()
        win2.refresh()
        win3.refresh()

    curses.napms(200)

try:
    assert len(sys.argv) > 1, 'Especificar puerto'
    port = sys.argv[1]
    ser = serial.Serial(port, timeout=250/1000)
    ser.flushInput()
    curses.wrapper(monitor)
    ser.close()
except AssertionError as msg:
    print(msg)
except:
    print('Error al leer el puerto')
    raise
    ser.close()
