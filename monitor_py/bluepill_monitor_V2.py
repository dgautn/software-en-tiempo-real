#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#
# script Python: bluepill_monitor.py
# (c) 2021, Gustavo AlbarrAn

import sys
import curses
import serial

def monitor(screen):
    cpu = 0
    task = 0
    time = 0
    up = 0

    begin_x = 20
    begin_y = 7
    height = 11
    width = 35
    
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_CYAN)
    curses.curs_set(False)
    
    win = curses.newwin(height, width, begin_y, begin_x)
    win.bkgd(' ', curses.color_pair(1))
    win.box()
    win.nodelay(True)   # no espera en getch()
    win.addstr(6, 2,"    Tasks:", curses.A_BOLD)
    win.addstr(8, 2,"CPU usage:", curses.A_BOLD)
    
    while(True):
        ser_bytes = ser.readline()
        ser_str = ser_bytes.decode('UTF-8')

        if 'OSTaskCtr' in ser_str:
            x = ser_str.index(':')
            task = ser_str[x + 1 : -1]
            task = int(task)

        if 'OSCPUUsage' in ser_str:
            x = ser_str.index(':')
            cpu = ser_str[x + 1 : -1]
            cpu = int(cpu)

        if 'Time' in ser_str:
            x = ser_str.index(':')
            time = ser_str[x + 1 : -1]
            time = int(time)

        if 'Up' in ser_str:
            x = ser_str.index(':')
            up = ser_str[x + 1 : -1]
            up = int(up)

        c = win.getch()
        if c != -1:
            if chr(c) == 'q':
                break
            if chr(c).lower() in ['s', 'r', 'u', 'd']:
                ser.write(bytes(chr(c), 'utf-8'))
 
        n = cpu // 10

        if n >= 4:
            n0bkg = curses.A_REVERSE
        else:
            n0bkg = curses.A_NORMAL

        if n >= 5:
            n1bkg = curses.A_REVERSE
        else:
            n1bkg = curses.A_NORMAL

        if n >= 6:
            pbkg = curses.A_REVERSE
        else:
            pbkg = curses.A_NORMAL
        
        minu = int(time / 60 // 1000)
        seg = int(time // 1000 - (minu * 60))
        mseg = int(time % 1000)
        win.addstr(4, 14, '{:02d}:{:02d}:{:d}'.format(minu, seg, mseg // 100), curses.A_BOLD)
        win.addstr(4, 16, ':', curses.A_BLINK | curses.A_BOLD)
        win.addstr(4, 19, ':', curses.A_BLINK | curses.A_BOLD)
        win.addstr(6, 14, str(task))
        win.addstr(8,14,"0%|__________|100%")
        win.addnstr(8,17,'          ', n, curses.A_REVERSE)
        win.addstr(8,20,'{:2d}'.format(cpu)[0],n0bkg)
        win.addstr(8,21,'{:2d}'.format(cpu)[1],n1bkg)
        win.addstr(8,22,'%',pbkg)
        win.addstr(2, 12, ' Ascendente' if up else 'Descendente', curses.A_BOLD)
        win.refresh()

    curses.napms(200)

try:
    assert len(sys.argv) > 1, 'Especificar puerto'
    port = sys.argv[1]
    ser = serial.Serial(port, timeout=250/1000)
    ser.flushInput()
    curses.wrapper(monitor)
    ser.close()
except AssertionError as msg:
    print(msg)
except:
    print('Error al leer el puerto')
    raise
    ser.close()
