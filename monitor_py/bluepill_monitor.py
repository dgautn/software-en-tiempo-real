#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# script Python: bluepill_monitor.py
# (c) 2021, Gustavo AlbarrAn

import sys
import curses
import serial

def monitor(screen):
    cpu = 0
    task = 0
    time = 0
    disc = 0

    begin_x = 20
    begin_y = 7
    height = 9
    width = 35
    
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_CYAN)
    curses.curs_set(False)
    
    win = curses.newwin(height, width, begin_y, begin_x)
    win.bkgd(' ', curses.color_pair(1))
    win.box()
    win.nodelay(True)   # no espera en getch()
    win.addstr(4, 2,"    Tasks:", curses.A_BOLD)
    win.addstr(6, 2,"CPU usage:", curses.A_BOLD)
    win.addstr(4, 18, 'Discos:', curses.A_BOLD)
    
    while(True):
        ser_bytes = ser.readline()
        ser_str = ser_bytes.decode('UTF-8')

        if 'Discos' in ser_str:
            x = ser_str.index(':')
            x += 1
            disc = ser_str[x:-1]
            disc = int(disc)

        if 'OSTaskCtr' in ser_str:
            x = ser_str.index(':')
            x += 1
            task = ser_str[x:-1]
            task = int(task)

        if 'OSCPUUsage' in ser_str:
            x = ser_str.index(':')
            x += 1
            cpu = ser_str[x:-1]
            cpu = int(cpu)

        if 'Time' in ser_str:
            x = ser_str.index(':')
            x += 1
            time = ser_str[x:-1]
            time = int(time)

        c = win.getch()
        if c != -1:
            if chr(c) == 'q':
                break
            if chr(c).lower() in ['s', 'r', 'u', 'd', 'k', 'p'] + [chr(x) for x in range( ord('1'), ord('9') + 1 )]:
                ser.write(bytes(chr(c), 'utf-8'))
 
        n = cpu // 10

        if n >= 4:
            n0bkg = curses.A_REVERSE
        else:
            n0bkg = curses.A_NORMAL

        if n >= 5:
            n1bkg = curses.A_REVERSE
        else:
            n1bkg = curses.A_NORMAL

        if n >= 6:
            pbkg = curses.A_REVERSE
        else:
            pbkg = curses.A_NORMAL
        
        minu = int(time / 60 // 1000)
        seg = int(time // 1000 - (minu * 60))
        mseg = int(time % 1000)
        win.addstr(2, 14, '{:02d}:{:02d}:{:d}'.format(minu, seg, mseg // 100), curses.A_BOLD)
        win.addstr(2, 16, ':', curses.A_BLINK | curses.A_BOLD)
        win.addstr(2, 19, ':', curses.A_BLINK | curses.A_BOLD)
        win.addstr(4, 14, str(task))
        win.addstr(6,14,"0%|__________|100%")
        win.addnstr(6,17,'          ', n, curses.A_REVERSE)
        win.addstr(6,20,'{:2d}'.format(cpu)[0],n0bkg)
        win.addstr(6,21,'{:2d}'.format(cpu)[1],n1bkg)
        win.addstr(6,22,'%',pbkg)
        win.addstr(4, 26, '{:d}'.format(disc))
        win.refresh()

    curses.napms(500)


try:
    assert len(sys.argv) > 1, 'Especificar puerto'
    port = sys.argv[1]
    ser = serial.Serial(port)
    ser.flushInput()
    curses.wrapper(monitor)
    ser.close()
except AssertionError as msg:
    print(msg)
except:
    print('Error al leer el puerto')
    raise
    ser.close()
