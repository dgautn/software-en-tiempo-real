/*
 * UsbSerial.c
 *
 *  Created on: Apr 16, 2020
 *      Author: esteban
 */

/********************************************************/
/*      usar version modificada de usbd_cdc_if.c        */
/********************************************************/

//#include "main.h"
#include <usbd_cdc_if.h>
#include "UsbSerial.h"
#include "stdarg.h"

FIFO RX_FIFO = {.head=0, .tail=0};

/*
**************************************************************************************************************************
*                                               UsbPrintfMsg()
*
* Description :
* Argument(s) : none
* Return(s)   : none.
* Caller(s)   :
* Note(s)     : none.
**************************************************************************************************************************
*/
void UsbPrintf (char  *p_fmt, ...)
{
	char           str[80u + 1u];
	unsigned  int  len;
    va_list        vArgs;

    va_start(vArgs, p_fmt);

    vsprintf((char       *)str,
             (char const *)p_fmt,
                           vArgs);

    va_end(vArgs);

    len = strlen(str);

    CDC_Transmit_FS((uint8_t *)str, len);
}



/*
**************************************************************************************************************************
*                                               VCP_read()
*
* Description :
* Argument(s) : none
* Return(s)   : none.
* Caller(s)   :
* Note(s)     : none.
**************************************************************************************************************************
*/

uint8_t UsbRead(uint8_t* Buff, uint32_t Len)
{
  uint32_t count=0;
  if ((Buff == NULL) || (Len == 0))                    /* Verificación de los datos de entrada */
   return 0;

  while (Len--)
   {
	if (RX_FIFO.head==RX_FIFO.tail) return count;     /* Lectura del Dato en el Buffer.        */
    count++;
    *Buff++=RX_FIFO.data[RX_FIFO.tail];
    RX_FIFO.tail=FIFO_INCR(RX_FIFO.tail);
   }

  return count;
}
