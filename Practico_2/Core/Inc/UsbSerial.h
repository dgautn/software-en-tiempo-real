/*
 * UsbSerial.h
 *
 *  Created on: Apr 15, 2020
 *      Author: esteban
 */

#ifndef INC_USBSERIAL_H_
#define INC_USBSERIAL_H_

#define  FIFO_SIZE 32  // must be 2^N

#define FIFO_INCR(x) (((x)+1)&((FIFO_SIZE)-1))

/* Structure of FIFO*/

typedef struct FIFO
 {
   uint32_t head;
   uint32_t tail;
   uint8_t data[FIFO_SIZE];
} FIFO;


void    UsbPrintf 		(char  *p_fmt, ...);
uint8_t UsbRead         (uint8_t* Buff, uint32_t Len);


#endif /* INC_USBSERIAL_H_ */
